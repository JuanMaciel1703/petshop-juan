<?php
  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_associacao.php');

  $pet = $_POST['pesquisa'];

  $dbAssociacao = new db_associacao();

  $servicos_associados = $dbAssociacao->getAssociacoesByPet($pet);
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Clientes</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
    <script src="/PetShop/assets/vendor/JqueryMask/jquery.mask.min.js" charset="utf-8"></script>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <div class="container">

      <div class="page-header">
        <h1><i class="fa fa-sitemap"></i> Serviços Associados</h1>
      </div>
      <hr>

      <?php if($servicos_associados): ?>

      <div class="row">
        <div class="col-md-12">
          <form action="/PetShop/app/controllers/associacao/buscar.php" method="post">
            <div class="row">
              <div class="col-md-1">
                <label for="pesquisa" class="my-2">Pesquisar: </label>
              </div>
              <div class="col-md-6">
                <select class="form-control" name="pesquisa">
                  <option value="">Selecione um Pet</option>
                  <?php foreach ($pets as $key => $pet): ?>
                    <option value="<?php echo $pet['pet_codigo'] ?>"><?php echo $pet['pet_nome'] ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-md-2">
                <button type="submit" name="button" class="btn btn-success"><i class="fa fa-search"></i> Pesquisar</button>
              </div>
              <div class="col-md-3">
                <a href="associar_servico.php" class="btn btn-secondary pull-right"><i class="fa fa-plus"></i> Associar Serviço</a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <hr>


      <div class="row">

        <table class="table table-responsive-md table-hover">
          <thead>
            <tr>
              <th>Pet</th>
              <th>Serviço</th>
              <th>Preço</th>
              <th class="text-center">Ação</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($servicos_associados as $key => $associacao):?>
              <tr>
                <td><?php echo $associacao['pet']['pet_nome'] ?></td>
                <td><?php echo $associacao['servico']['srv_descricao'] ?></td>
                <td> <span>R$</span> <?php echo $associacao['servico']['srv_preco'] ?></td>
                <td class="text-center">
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-warning" onclick="abreModalEditarServicoAssociado('<?php echo $associacao['pet']['pet_codigo'] ?>', '<?php echo $associacao['servico']['srv_codigo'] ?>')" > <i class="fa fa-edit text-white"></i> </a>
                    <a class="btn btn-danger excluir" data-toggle="modal" onclick="abreModalExcluir('<?php echo $associacao['pet_codigo'] ?>', '<?php echo $associacao['srv_codigo'] ?>')"> <i class="fa fa-trash text-white"></i> </a>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
            <?php else: ?>
                <p class="text-center">Nenhuma Associação Encontrada.</p>
                <div class="text-center">
                  <a href="associar_servico.php" class="btn btn-secondary"><i class="fa fa-plus"> </i>  Associar Serviço</a>
                </div>
            <?php endif; ?>

          </tbody>
        </table>
      </div>
    </div>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>


    <div id="modalExcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelExcluir" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Excluir Serviço Associado</h4>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>Deseja realmente excluir o registro?</p>
          </div>
          <div class="modal-footer">
            <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
            <button type="submit" class="btn btn-danger pull-right">Excluir</button>
          </div>
        </div>
      </div>
    </div>

    <div id="modalEditarServicoAssociado" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEditar" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Editar Serviço Associado</h4>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form>
            <div class="modal-body">
                <?php require_once(BASE_PATH.'/app/views/form_servicoAssociado.php') ?>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
              <button type="submit" class="btn btn-warning pull-right">Atualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">

      function abreModalEditarServicoAssociado(id, pet, servico) {
        $('#modalEditarServicoAssociado').modal('show');

      }

      function abreModalExcluir(id){
        $('#modalExcluir').modal('show');
      }
    </script>
  </body>
</html>
