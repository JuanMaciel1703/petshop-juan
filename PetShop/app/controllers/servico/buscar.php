<?php
  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_servico.php');

  $codigo = $_POST['pesquisa'];

  $dbServico = new db_servico();

  $response = $dbServico->getServico($codigo);
?>

<!DOCTYPE html>
<html>
  <head>
    <title>Serviços</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <div class="container">
      <div class="page-header">
        <h1><i class="fa fa-tasks"></i> Serviços</h1>
      </div>
      <hr>
      <?php if($response): ?>

      <div class="row">
        <div class="col-md-12">
          <form action="/PetShop/app/controllers/servico/buscar.php" method="post">
            <div class="row">
              <div class="col-md-1">
                <label for="pesquisa" class="my-2">Pesquisar: </label>
              </div>
              <div class="col-md-6">
                <input class="form-control" type="text" name="pesquisa" placeholder="Codigo">
              </div>
              <div class="col-md-2">
                <button type="submit" name="button" class="btn btn-success"><i class="fa fa-search"></i> Pesquisar</button>
              </div>
              <div class="col-md-3">
                <a href="cadastro_servico.php" class="btn btn-secondary pull-right"><i class="fa fa-plus"></i> Cadastrar Serviço</a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <hr>


          <div class="row">
            <table class="table table-responsive-md table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Descrição</th>
                  <th>Preço</th>
                  <th class="text-center">Ação</th>
                </tr>
              </thead>
              <tbody>
                  <tr>
                    <td><?php echo $response['srv_codigo'] ?></td>
                    <td><?php echo $response['srv_descricao'] ?></td>
                    <td><?php echo $response['srv_preco'] ?></td>
                    <td class="text-center">
                      <div class="btn-group" role="group" aria-label="Basic example">
                        <a class="btn btn-warning" data-toggle="modal" onclick="abreModalEditarServico('<?php echo $response['srv_codigo'] ?>', '<?php echo $response['srv_descricao'] ?>', '<?php echo $response['srv_preco'] ?>')"> <i class="fa fa-edit text-white"></i> </a>
                        <a class="btn btn-danger excluir" data-toggle="modal" onclick="abreModalExcluir('<?php echo $response['srv_codigo'] ?>')"> <i class="fa fa-trash text-white"></i> </a>
                      </div>
                    </td>
                  </tr>
                <?php else: ?>
                    <p class="text-center">Nenhumm serviço encontrado.</p>
                    <div class="text-center">
                      <a href="cadastro_servico.php" class="btn btn-secondary"><i class="fa fa-plus"> </i>  Cadastrar Serviço</a>
                    </div>
                <?php endif; ?>
              </tbody>
            </table>
          </div>
        </div>

        <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>

        <div id="modalExcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelExcluir" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

            </div>
          </div>
        </div>

        <div id="modalEditarServico" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEditar" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Editar Serviço</h4>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form>
                <div class="modal-body">
                    <?php require_once(BASE_PATH.'/app/views/form_servico.php') ?>
                </div>
                <div class="modal-footer">
                  <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
                  <button type="submit" class="btn btn-warning pull-right">Atualizar</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          function abreModalEditarServico(id, descricao, preco) {
            $('#modalEditarServico').modal('show');
            $('#descricao').val(descricao);
            $('#preco').val(preco);
          }

          function abreModalExcluir(id){
            $('#modalExcluir').modal('show');
            $(".modal-content").load('modalExcluir.php?id=' + id + '&type=servico');
          }

  </body>
</html>
