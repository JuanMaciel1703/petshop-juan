<?php

  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_servico.php');

  $dados['descricao'] = $_POST['descricao'];
  $dados['preco'] = $_POST['preco'];

  $dbServico = new db_servico();

  $response = $dbServico->save($dados);

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cadastro de Serviço</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <?php if($response): ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Sucesso</h1>
          <h3>Cadastro Realizado com Sucesso!</h3>
          <a href="/PetShop/app/views/servicos.php" class="btn btn-primary">Voltar</a>
        </div>
        <hr>
      </div>
    <?php else: ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Falha</h1>
          <br>
          <h3>Não foi possível completar o cadastro. Verifique as informações e tente novamente.</h3>
          <br>
          <a href="/PetShop/app/views/cadastro_servico.php" class="btn btn-danger">Voltar</a>
        </div>
        <hr>
      </div>

    <?php endif; ?>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
  </body>
</html>
