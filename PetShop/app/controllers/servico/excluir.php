<?php
  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_servico.php');

  $codigo = $_POST['srv_codigo_excluir'];

  $dbServico = new db_servico();

  $hasAssociacao = $dbServico->hasAssociacao($codigo);
  if($hasAssociacao){
    $response = $dbServico->forceDelete($codigo);
  } else {
    $response = $dbServico->delete($codigo);
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Remoção de Servico</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <?php if($response): ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Sucesso</h1>
          <h3>Remoção Realizada com Sucesso!</h3>
          <a href="/PetShop/app/views/servicos.php" class="btn btn-primary">Voltar</a>
        </div>
        <hr>
      </div>
    <?php else: ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Falha</h1>
          <br>
          <h3>Não foi possível completar a remoção.</h3>
          <br>
          <a href="/PetShop/app/views/servicos.php" class="btn btn-danger">Voltar</a>
        </div>
        <hr>
      </div>

    <?php endif; ?>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
  </body>
</html>
