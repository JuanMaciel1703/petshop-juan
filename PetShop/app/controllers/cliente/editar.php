<?php

  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_cliente.php');

  $dados['nome'] = $_POST['nome'];
  $dados['endereco'] = $_POST['endereco'];
  $dados['cpf'] = $_POST['cpf'];
  $dados['telefone'] = $_POST['telefone'];

  $dbCliente = new db_cliente();

  $response = $dbCliente->update($dados);

 ?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Atualização de Cliente</title>
     <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
   </head>
   <body>
     <?php require_once(BASE_PATH.'/base/header.php'); ?>

     <?php if($response): ?>
       <div class="container text-center">
         <div class="page-header">
           <h1>Sucesso</h1>
           <h3>Atualização Realizado com Sucesso!</h3>
           <a href="/PetShop/app/views/clientes.php" class="btn btn-primary">Voltar</a>
         </div>
         <hr>
       </div>
     <?php else: ?>
       <div class="container text-center">
         <div class="page-header">
           <h1>Falha</h1>
           <br>
           <h3>Não foi possível completar a Atualização. Verifique as informações e tente novamente.</h3>
           <br>
           <a href="/PetShop/app/views/clientes.php" class="btn btn-danger">Voltar</a>
         </div>
         <hr>
       </div>

     <?php endif; ?>

     <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
   </body>
 </html>
