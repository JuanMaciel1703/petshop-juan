<?php
  include('../../../init.php');
  include(BASE_PATH.'/app/db/db_pet.php');

  $codigo = $_POST['pet_codigo_excluir'];

  $dbPet = new db_pet();

  $hasAssociacao = $dbPet->hasAssociacao($codigo);
  if($hasAssociacao){
    $response = $dbPet->forceDelete($codigo);
  } else {
    $response = $dbPet->delete($codigo);
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Remoção de Pet</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <?php if($response): ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Sucesso</h1>
          <h3>Remoção Realizada com Sucesso!</h3>
          <a href="/PetShop/app/views/pets.php" class="btn btn-primary">Voltar</a>
        </div>
        <hr>
      </div>
    <?php else: ?>
      <div class="container text-center">
        <div class="page-header">
          <h1>Falha</h1>
          <br>
          <h3>Não foi possível completar a remoção.</h3>
          <br>
          <a href="/PetShop/app/views/pets.php" class="btn btn-danger">Voltar</a>
        </div>
        <hr>
      </div>

    <?php endif; ?>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
  </body>
</html>
