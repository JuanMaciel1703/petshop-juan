<?php

require_once(BASE_PATH.'/app/db/DB.php');

class db_pet {
  private $conn;

  public function __construct()
  {
    $this->conn = conectar();
  }

  public function getPets()
  {
      try {
        $sql = "SELECT * FROM pet";
        $stm = $this->conn->prepare($sql);
        $stm->execute();

        $response = $stm->fetchAll();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }
  }

  public function getPet($codigo)
  {
      try {
        $sql = "SELECT * FROM pet WHERE pet_codigo = :codigo";
        $stm = $this->conn->prepare($sql);
        $stm->bindValue(':codigo', $codigo);
        $stm->execute();

        $response = $stm->fetch();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function hasAssociacao($petCodigo)
  {
    try {
      $sql = "SELECT * FROM pet_servico WHERE pet_codigo = :codigo";
      $stm = $this->conn->prepare($sql);
      $stm->bindValue(':codigo', $petCodigo);
      $stm->execute();

      $response = $stm->fetch();

      return $response;
    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function save($dados)
  {
    try {
      $sql = "INSERT INTO pet VALUES(NULL, :raca, :data_nascimento, :nome, :cpf)";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':raca', $dados['raca']);
      $stm->bindValue(':data_nascimento', $dados['data_nascimento']);
      $stm->bindValue(':nome', $dados['nome']);
      $stm->bindValue(':cpf', $dados['cliente']);

      $stm->execute();

      return true;

    } catch (PDOException $e) {
      echo "<scrip>console.log(".$e.")</script>";
      return false;
    }
  }

  public function update($dados)
  {
    try {
      $sql = "UPDATE pet SET pet_nome = :pet_nome, pet_raca = :pet_raca, pet_data_nascimento = :pet_data_nascimento WHERE pet_codigo = :pet_codigo";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':pet_codigo', $dados['pet_codigo']);
      $stm->bindValue(':pet_raca', $dados['pet_raca']);
      $stm->bindValue(':pet_nome', $dados['pet_nome']);
      $stm->bindValue(':pet_data_nascimento', $dados['pet_data_nascimento']);

      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function deletePets($pets)
  {
    try {
      $query = "DELETE FROM pet WHERE pet_codigo in (".str_repeat("?,", count($pets) - 1)."?)";
      $stm = $this->conn->prepare($query);
      $stm->execute($pets);

      if($stm->rowCount() > 0){
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function forceDelete($codigo)
  {
    try {
      $query = "DELETE FROM pet_servico WHERE pet_codigo = :codigo";
      $stm = $this->conn->prepare($query);
      $stm->bindValue(':codigo', $codigo);
      $stm->execute();

      if($stm->rowCount() > 0){

        if (self::delete($codigo)) {
          return true;
        }

      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function delete($codigo)
  {
    try {
      $query = "DELETE FROM pet WHERE pet_codigo = :codigo";
      $stm = $this->conn->prepare($query);
      $stm->bindValue(':codigo', $codigo);
      $stm->execute();

      if($stm->rowCount() > 0){
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }
}
