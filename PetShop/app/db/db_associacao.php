<?php

require_once(BASE_PATH.'/app/db/DB.php');

class db_associacao {
  private $conn;

  public function __construct()
  {
    $this->conn = conectar();
  }

  public function getAssociacoes()
  {
      try {
        $sql = "SELECT * FROM pet_servico";
        $stm = $this->conn->prepare($sql);
        $stm->execute();

        $associacoes= $stm->fetchAll();

        if(count($associacoes) > 0) {
          $response = array();

          foreach ($associacoes as $key => $associacao) {
            $sql = "SELECT * FROM servico WHERE srv_codigo = :srv_codigo";
            $stm = $this->conn->prepare($sql);
            $stm->bindValue(':srv_codigo', $associacao['srv_codigo']);
            $stm->execute();

            $response[$key]['servico'] = $stm->fetch();


            $sql = "SELECT * FROM pet WHERE pet_codigo = :pet_codigo";
            $stm = $this->conn->prepare($sql);
            $stm->bindValue(':pet_codigo', $associacao['pet_codigo']);
            $stm->execute();

            $response[$key]['pet'] = $stm->fetch();

          }
          return $response;
        }
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function getAssociacoesByPet($pet)
  {

      try {
        $sql = "SELECT * FROM pet_servico WHERE pet_codigo = :codigo";
        $stm = $this->conn->prepare($sql);
        $stm->bindValue(':codigo', $pet);
        $stm->execute();

        $associacoes= $stm->fetchAll();

        if(count($associacoes) > 0) {
          $response = array();

          foreach ($associacoes as $key => $associacao) {
            $sql = "SELECT * FROM servico WHERE srv_codigo = :srv_codigo";
            $stm = $this->conn->prepare($sql);
            $stm->bindValue(':srv_codigo', $associacao['srv_codigo']);
            $stm->execute();

            $response[$key]['servico'] = $stm->fetch();


            $sql = "SELECT * FROM pet WHERE pet_codigo = :pet_codigo";
            $stm = $this->conn->prepare($sql);
            $stm->bindValue(':pet_codigo', $associacao['pet_codigo']);
            $stm->execute();

            $response[$key]['pet'] = $stm->fetch();

          }

          return $response;



        }
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function save($dados)
  {
    try {
      $sql = "INSERT INTO pet_servico VALUES(:pet_codigo, :srv_codigo)";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':pet_codigo', $dados['pet_codigo']);
      $stm->bindValue(':srv_codigo', $dados['srv_codigo']);

      $stm->execute();

      return true;

    } catch (PDOException $e) {
      return false;
    }

  }

  public function update($dados)
  {
    try {
      $sql = "UPDATE pet_servico SET srv_codigo = :novo_servico, pet_codigo = :novo_pet WHERE srv_codigo = :srv_codigo AND pet_codigo = :pet_codigo";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':novo_servico', $dados['novo_servico']);
      $stm->bindValue(':novo_pet', $dados['novo_pet']);
      $stm->bindValue(':srv_codigo', $dados['srv_codigo']);
      $stm->bindValue(':pet_codigo', $dados['pet_codigo']);

      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function deleteAssociacoes($pets)
  {
    try {
      foreach($pets as $key => $pet) {
        $query = "DELETE FROM pet_servico WHERE pet_codigo = :codigo";

        $stm = $this->conn->prepare($query);
        $stm->bindValue(':codigo', $pet);
        $stm->execute();
      }

      return true;

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function delete($petCodigo, $srvCodigo)
  {
    try {
      $query = "DELETE FROM pet_servico WHERE pet_codigo = :pet AND srv_codigo = :servico";

      $stm = $this->conn->prepare($query);
      $stm->bindValue(':pet', $petCodigo);
      $stm->bindValue(':servico', $srvCodigo);
      $stm->execute();

      if($stm->rowCount() > 0){
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }
}
