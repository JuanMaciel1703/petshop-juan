<?php

function conectar()
{
  try {
    $conn = new PDO('mysql:host='.HOST.';dbname='.DBNAME, USER, PASS,  array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $conn;

  } catch(PDOException $e) {
      echo 'ERROR: ' . $e->getMessage();
  }
}
