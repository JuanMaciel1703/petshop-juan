<?php

require_once(BASE_PATH.'/app/db/DB.php');
require_once(BASE_PATH.'/app/db/db_pet.php');
require_once(BASE_PATH.'/app/db/db_associacao.php');

class db_cliente {
  private $conn;

  public function __construct()
  {
    $this->conn = conectar();
  }

  public function getClientes()
  {
      try {
        $sql = "SELECT * FROM cliente";
        $stm = $this->conn->prepare($sql);
        $stm->execute();

        $response = $stm->fetchAll();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function getCliente($cpf)
  {
      try {
        $sql = "SELECT * FROM cliente WHERE cli_cpf = :cpf";
        $stm = $this->conn->prepare($sql);
        $stm->bindValue(':cpf', $cpf);
        $stm->execute();

        $response = $stm->fetch();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function hasPet($cpf)
  {
    try {
      $sql = "SELECT * FROM pet WHERE cli_cpf = :cpf";
      $stm = $this->conn->prepare($sql);
      $stm->bindValue(':cpf', $cpf);
      $stm->execute();

      $response = $stm->fetchAll();


      if(count($response) > 0) {
        return $response;
      } else {
        return false;
      }
    } catch (PDOException $e) {
      echo $e;
      return false;
    }

  }

  public function save($dados)
  {
    try {
      $sql = "INSERT INTO cliente VALUES(:cpf, :nome, :endereco, :telefone)";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':cpf', $dados['cpf']);
      $stm->bindValue(':nome', $dados['nome']);
      $stm->bindValue(':endereco', $dados['endereco']);
      $stm->bindValue(':telefone', $dados['telefone']);

      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      return false;
    }
  }

  public function update($dados)
  {
    try {
      $sql = "UPDATE cliente SET cli_nome = :nome, cli_endereco = :endereco, cli_telefone = :telefone WHERE cli_cpf = :cpf";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':cpf', $dados['cpf']);
      $stm->bindValue(':nome', $dados['nome']);
      $stm->bindValue(':endereco', $dados['endereco']);
      $stm->bindValue(':telefone', $dados['telefone']);

      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function delete($cpf)
  {
    try {
      $sql = "DELETE FROM cliente WHERE cli_cpf = :cpf";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':cpf', $cpf);
      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      return false;
    }
  }

  public function forceDelete($cpf)
  {
    try {
      $dbPet = new db_pet();
      $dbAssociacao = new db_associacao();

      $tmp = $this->hasPet($cpf);

      $pets = array();

      foreach($tmp as $key => $pet) {
        $pets[] = (int) $pet['pet_codigo'];
      }

      if(!$dbAssociacao->deleteAssociacoes($pets)){
        return false;
      }

      if(!$dbPet->deletePets($pets)){
        return false;
      }

      $sql = "DELETE FROM cliente WHERE cli_cpf = :cpf";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':cpf', $cpf);
      $stm->execute();

      return true;

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

}
