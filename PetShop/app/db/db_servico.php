<?php

require_once(BASE_PATH.'/app/db/DB.php');

class db_servico {
  private $conn;

  public function __construct()
  {
    $this->conn = conectar();
  }

  public function getServicos()
  {
      try {
        $sql = "SELECT * FROM servico";
        $stm = $this->conn->prepare($sql);
        $stm->execute();

        $response = $stm->fetchAll();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }

  }

  public function getServico($codigo)
  {
      try {
        $sql = "SELECT * FROM servico WHERE srv_codigo = :codigo";
        $stm = $this->conn->prepare($sql);
        $stm->bindValue(':codigo', $codigo);
        $stm->execute();

        $response = $stm->fetch();

        return $response;
      } catch (PDOException $e) {
        echo $e;
      }
  }

  public function hasAssociacao($srvCodigo)
  {
    try {
      $sql = "SELECT * FROM pet_servico WHERE srv_codigo = :codigo";
      $stm = $this->conn->prepare($sql);
      $stm->bindValue(':codigo', $srvCodigo);
      $stm->execute();

      $response = $stm->fetch();

      return $response;
    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function save($dados)
  {
    try {
      $sql = "INSERT INTO servico VALUES(NULL, :preco, :descricao)";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':preco', $dados['preco']);
      $stm->bindValue(':descricao', $dados['descricao']);

      $stm->execute();

      return true;

    } catch (PDOException $e) {
      echo "<scrip>console.log(".$e.")</script>";
      return false;
    }

  }

  public function update($dados)
  {
    try {
      $sql = "UPDATE servico SET srv_descricao = :descricao, srv_preco = :preco WHERE srv_codigo = :codigo";
      $stm = $this->conn->prepare($sql);

      $stm->bindValue(':descricao', $dados['srv_descricao']);
      $stm->bindValue(':preco', $dados['srv_preco']);
      $stm->bindValue(':codigo', $dados['srv_codigo']);

      $stm->execute();

      if ($stm->rowCount() > 0) {
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function forceDelete($codigo)
  {
    try {
      $query = "DELETE FROM pet_servico WHERE srv_codigo = :codigo";
      $stm = $this->conn->prepare($query);
      $stm->bindValue(':codigo', $codigo);
      $stm->execute();

      if($stm->rowCount() > 0){

        if (self::delete($codigo)) {
          return true;
        }

      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }

  public function delete($codigo)
  {
    try {
      $query = "DELETE FROM servico WHERE srv_codigo = :codigo";
      $stm = $this->conn->prepare($query);
      $stm->bindValue(':codigo', $codigo);
      $stm->execute();

      if($stm->rowCount() > 0){
        return true;
      } else {
        return false;
      }

    } catch (PDOException $e) {
      echo $e;
      return false;
    }
  }
}
