  <fieldset class="form-group">
    <label for="exampleInputEmail1">Nome Completo <i class="text-danger">*</i></label>
    <input id="nome" type="text" class="form-control" placeholder="Nome" name="nome" required>
    <small class="text-muted">Digite o nome completo do Cliente</small>
  </fieldset>

  <fieldset class="form-group">
    <label for="exampleInputPassword1">Endereço <i class="text-danger">*</i></label>
    <input id="endereco" type="text" class="form-control" placeholder="Endereço" name="endereco" required>
    <small class="text-muted">Digite o endereço completo do Cliente</small>
  </fieldset>

  <div class="row">
    <div class="col-md-6">
      <label for="exampleInputPassword1">CPF <i class="text-danger">*</i></label>
      <input type="text" class="form-control" placeholder="000.000.000-00" name="cpf" id="cpf" required>
      <small class="text-muted">Digite o CPF do cliente</small>

      <script type="text/javascript">
        $(document).ready(function() {
          $('#cpf').mask('000.000.000-00', {reverse: true});
        });
      </script>
    </div>

    <div class="col-md-6">
      <label for="exampleInputPassword1">Telefone <i class="text-danger">*</i></label>
      <input id="telefone" type="text" class="form-control" placeholder="(00)0000-0000" name="telefone" required>
      <small class="text-muted">Digite o Telefone do Cliente</small>

      <script type="text/javascript">
        $(document).ready(function() {
          $('#telefone').mask('(00)0000-0000');
        });
      </script>
    </div>
  </div>
