<?php include '../../init.php'; ?>

<?php
  require_once(BASE_PATH.'/app/db/db_cliente.php');
  $dbCliente = new db_cliente();

  $clientes = $dbCliente->getClientes();
 ?>


<!DOCTYPE html>
<html>
  <head>
    <title>Clientes</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
    <script src="/PetShop/assets/vendor/JqueryMask/jquery.mask.min.js" charset="utf-8"></script>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <div class="container">
      <div class="page-header">
        <h1><i class="fa fa-user-circle"></i> Clientes</h1>
      </div>
      <hr>
      <?php if(count($clientes) > 0): ?>
      <div class="row">
        <div class="col-md-12">
          <form action="/PetShop/app/controllers/cliente/buscar.php" method="post">
            <div class="row">
              <div class="col-md-1">
                <label for="pesquisa" class="my-2">Pesquisar: </label>
              </div>
              <div class="col-md-6">
                <input id="cpf_pesquisa" class="form-control" type="text" name="pesquisa" value="" placeholder="000.000.000-00">
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('#cpf_pesquisa').mask('000.000.000-00');
                  });
                </script>
              </div>
              <div class="col-md-2">
                <button type="submit" name="button" class="btn btn-success"><i class="fa fa-search"></i> Pesquisar</button>
              </div>
              <div class="col-md-3">
                <a href="cadastro_cliente.php" class="btn btn-secondary pull-right"><i class="fa fa-plus"> </i>  Cadastrar Cliente</a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <hr>

      <div class="row">


        <table class="table table-responsive-md table-hover">
          <thead>
            <tr>
              <th>CPF</th>
              <th>Nome</th>
              <th>Endereço</th>
              <th>Telefone</th>
              <th class="text-center">Ação</th>
            </tr>
          </thead>
          <tbody>
              <?php foreach ($clientes as $key => $cliente):?>
                <tr>
                  <td><?php echo $cliente['cli_cpf'] ?></td>
                  <td><?php echo $cliente['cli_nome'] ?></td>
                  <td><?php echo $cliente['cli_endereco'] ?></td>
                  <td><?php echo $cliente['cli_telefone'] ?></td>
                  <td class="text-center">
                    <div class="btn-group" role="group" aria-label="Basic example">
                      <a class="btn btn-warning editar" data-toggle="modal" onclick="abreModalEditarCliente('<?php echo $cliente['cli_nome'] ?>','<?php echo $cliente['cli_cpf'] ?>', '<?php echo $cliente['cli_endereco'] ?>', '<?php echo $cliente['cli_telefone'] ?>')"> <i class="fa fa-edit text-white"></i> </a>
                      <a class="btn btn-danger excluir" data-toggle="modal" onclick="abreModalExcluir('<?php echo $cliente['cli_cpf'] ?>')"> <i class="fa fa-trash text-white"></i> </a>
                    </div>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>

            <?php else: ?>
                <p class="text-center">Não Há Nenhum Cliente Cadastrado.</p>
                <div class="text-center">
                  <a href="cadastro_cliente.php" class="btn btn-secondary"><i class="fa fa-plus"> </i>  Cadastrar Cliente</a>
                </div>
            <?php endif; ?>
        </table>
      </div>
    </div>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>

    <div id="modalExcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelExcluir" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
      </div>
    </div>


    <div id="modalEditarCliente" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEditar" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Editar Cliente</h4>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/PetShop/app/controllers/cliente/editar.php" method="post">
            <div class="modal-body">
                <?php require_once(BASE_PATH.'/app/views/form_cliente.php') ?>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
              <button type="submit" class="btn btn-warning pull-right">Atualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      function abreModalEditarCliente(nome, cpf, endereco, telefone) {
        $('#modalEditarCliente').modal('show');
        $('#nome').val(nome);
        $('#cpf').val(cpf);
        $('#cpf').attr('readonly','readonly');
        $('#endereco').val(endereco);
        $('#telefone').val(telefone);
      }

      function abreModalExcluir(cpf){
        $('#modalExcluir').modal('show');
        $(".modal-content").load('modalExcluir.php?cpf=' + cpf + '&type=cliente', function(){
          $('#cpf_excluir').val(cpf);
        });
      }
    </script>

  </body>
</html>
