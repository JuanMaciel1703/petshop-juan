
<?php
  require_once(BASE_PATH.'/app/db/db_cliente.php');
  $dbCliente = new db_cliente();

  $clientes = $dbCliente->getClientes();
 ?>


<fieldset class="form-group">
  <label for="clientes">Cliente <span class="nome-dono"></span> <i class="text-danger">*</i> </label>
  <select class="form-control" name="clientes" id="cliente" required>
    <option value="">Selecione um Cliente</option>
    <?php foreach ($clientes as $key => $cliente): ?>
      <option value="<?php echo $cliente['cli_cpf']; ?>"><?php echo $cliente['cli_nome'] ?></option>
    <?php endforeach; ?>
  </select>
</fieldset>

<fieldset class="form-group" style="display: none;">
  <input id="pet_codigo_editar" type="text" class="form-control" name="pet_codigo">
</fieldset>

<fieldset class="form-group">
  <label for="nome">Nome <i class="text-danger">*</i></label>
  <input id="nome" type="text" class="form-control" placeholder="Nome" name="nome" required>
  <small class="text-muted">Digite o nome completo do Pet</small>
</fieldset>

<div class="row">
  <div class="col-md-6">
    <fieldset class="form-group">
      <label for="raca">Raça <i class="text-danger">*</i></label>
      <input id="raca" type="text" class="form-control" placeholder="Raça" name="raca" required>
      <small class="text-muted">Digite a raça do Pet</small>
    </fieldset>
  </div>

  <div class="col-md-6">
    <fieldset class="form-group">
      <label for="data_nascimento">Data Nascimento <i class="text-danger">*</i></label>
      <input id="data_nascimento" type="text" class="form-control" placeholder="0000/00/00" name="data_nascimento" required>
      <small class="text-muted">Digite a data de nascimento do Pet. <b>Formato: YYYY-MM-DD</b> </small>

      <script type="text/javascript">
        $(document).ready(function() {
          $('#data_nascimento').mask('0000-00-00');
        });
      </script>
    </div>
    </fieldset>
</div>
