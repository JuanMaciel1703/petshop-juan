<?php include '../../init.php'; ?>

<?php
  require_once(BASE_PATH.'/app/db/db_pet.php');
  $dbPet = new db_pet();

  $pets = $dbPet->getPets();
 ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Pets</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
    <script src="/PetShop/assets/vendor/JqueryMask/jquery.mask.min.js" charset="utf-8"></script>

  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>


    <div class="container">
      <div class="page-header">
        <h1><i class="fa fa-paw"></i> Pets</h1>
      </div>
      <hr>

      <div class="row">
        <div class="col-md-12">
          <form action="/PetShop/app/controllers/pet/buscar.php" method="post">
            <div class="row">
              <div class="col-md-1">
                <label for="pesquisa" class="my-2">Pesquisar: </label>
              </div>
              <div class="col-md-6">
                <input class="form-control" type="text" name="pet_codigo" value="" placeholder="Codigo do Pet">
              </div>
              <div class="col-md-2">
                <button type="submit" name="button" class="btn btn-success"><i class="fa fa-search"></i> Pesquisar</button>
              </div>
              <div class="col-md-3">
                <a href="cadastro_pet.php" class="btn btn-secondary pull-right"><i class="fa fa-plus"> </i> Cadastrar Pet</a>
              </div>
            </div>
          </form>
        </div>
      </div>
      <hr>

      <div class="row">
        <table class="table table-responsive-md table-hover">
          <thead>
            <tr>
              <th>ID</th>
              <th>Nome</th>
              <th>Dono</th>
              <th>Raça</th>
              <th>Data de Nascimento</th>
              <th class="text-center">Ação</th>
            </tr>
          </thead>
          <tbody>
            <?php
              require_once(BASE_PATH.'/app/db/db_cliente.php');
              $dbCliente = new db_cliente();
             ?>
            <?php foreach ($pets as $key => $pet):?>
              <?php $cliente = $dbCliente->getCliente($pet['cli_cpf']); ?>
              <tr>
                <td><?php echo $pet['pet_codigo'] ?></td>
                <td><?php echo $pet['pet_nome'] ?></td>
                <td><?php echo $cliente['cli_nome'] ?></td>
                <td><?php echo $pet['pet_raca'] ?></td>
                <td><?php echo $pet['pet_data_nascimento'] ?></td>
                <td class="text-center">
                  <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-warning" onclick="abreModalEditarPet('<?php echo $pet['pet_codigo'] ?>', '<?php echo $pet['cli_cpf'] ?>','<?php echo $pet['pet_nome'] ?>', '<?php echo $pet['pet_raca'] ?>', '<?php echo $pet['pet_data_nascimento'] ?>')"> <i class="fa fa-edit text-white"></i> </a>
                    <a class="btn btn-danger excluir" data-toggle="modal" onclick="abreModalExcluir('<?php echo $pet['pet_codigo'] ?>')"> <i class="fa fa-trash text-white"></i> </a>
                  </div>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>
      </div>
    </div>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>

    <div id="modalExcluir" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelExcluir" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

        </div>
      </div>
    </div>

    <div id="modalEditarPet" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEditar" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Editar Pet</h4>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <form action="/PetShop/app/controllers/pet/editar.php" method="post">
            <div class="modal-body">
                <?php require_once(BASE_PATH.'/app/views/form_pet.php') ?>
            </div>
            <div class="modal-footer">
              <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
              <button type="submit" class="btn btn-warning pull-right">Atualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>

    <script type="text/javascript">

      function abreModalEditarPet(id, cliente, nome, raca, dataNascimento) {
        $('#modalEditarPet').modal('show');
        $('#pet_codigo_editar').val(id);
        $('#cliente').val(cliente);
        $('#nome').val(nome);
        $('#cliente').hide(cliente);
        $('.nome-dono').text(': ' + cliente);
        $('#raca').val(raca);
        $('#data_nascimento').val(dataNascimento);

        console.log($('#pet_codigo_editar').val());
      }

      function abreModalExcluir(id){
        $('#modalExcluir').modal('show');
        $(".modal-content").load('modalExcluir.php?pet_codigo_excluir=' + id + '&type=pet', function(){
          $("#pet_codigo_excluir").val(id);
        });
      }
    </script>
  </body>
</html>
