<?php include '../../init.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Associar Serviço</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>

    <div class="container">
      <div class="page-header">
        <h1>Associar Serviço</h1>
      </div>
      <hr>
      <form action="/PetShop/app/controllers/associacao/cadastro.php" method="post">

        <?php require_once(BASE_PATH.'/app/views/form_servicoAssociado.php'); ?>

        <hr>
        <button type="submit" class="btn btn-success pull-right">Cadastrar</button>
        <button type="reset" class="btn btn-danger pull-right mx-2">Limpar</button>

      </form>
    </div>

    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
  </body>
</html>
