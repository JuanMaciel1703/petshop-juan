<?php
  include('../../init.php');
  include(BASE_PATH.'/app/db/db_cliente.php');
  require_once(BASE_PATH.'/app/db/db_servico.php');


?>

<?php if($_GET['type'] == 'cliente'): ?>
  <?php

  $dbCliente = new db_cliente();
  $cpf = $_GET['cpf'];

  $hasPet = $dbCliente->hasPet($cpf);

  ?>

  <?php if ($hasPet):?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Cliente</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>O Cliente selecionado possui Animais cadastrados no sistema e não é possível removê-lo. Caso desejar, é possível forçar a remoção excluindo juntamente os registros de animais e serviços associados ao cliente.</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/cliente/excluir.php" method="post">
      <input id="cpf_excluir" type="text" name="cpf" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Forçar Excluir</button>
    </form>
  </div>

  <?php else: ?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Cliente</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>Deseja realmente excluir o Cliente?</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/cliente/excluir.php" method="post">
      <input id="cpf_excluir" type="text" name="cpf" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Excluir</button>
    </form>
  </div>

  <?php endif; ?>
<?php endif; ?>




<?php if($_GET['type'] == 'pet'): ?>

  <?php

    $codigo = $_GET['pet_codigo_excluir'];
    $dbPet = new db_pet();
    $hasAssociacao = $dbPet->hasAssociacao($codigo);

   ?>

  <?php if ($hasAssociacao):?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Pet</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>O Pet selecionado possui serviços associados à ele, logo, não é possível realizar a exclusão do mesmo. Caso desejar, é possível forçar a exclusão removendo juntamente os registros de serviços desse pet.</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/pet/excluir.php" method="post">
      <input id="pet_codigo_excluir" type="text" name="pet_codigo_excluir" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Forçar Excluir</button>
    </form>
  </div>

  <?php else: ?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Pet</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>Deseja realmente excluir o Pet?</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/pet/excluir.php" method="post">
      <input id="pet_codigo_excluir" type="text" name="pet_codigo_excluir" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Excluir</button>
    </form>
  </div>

  <?php endif; ?>
<?php endif; ?>





<?php if($_GET['type'] == 'servico'): ?>

  <?php

    $codigo = $_GET['srv_codigo_excluir'];
    $dbServico = new db_servico();
    $hasAssociacao = $dbServico->hasAssociacao($codigo);

   ?>

  <?php if ($hasAssociacao):?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Serviço</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>O Serviço selecionado está associado à algum Pet. Para efetuar a remoção, é necessário remover todos os registros referentes ao serviço.</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/servico/excluir.php" method="post">
      <input id="srv_codigo_excluir" type="text" name="srv_codigo_excluir" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Excluir</button>
    </form>
  </div>

  <?php else: ?>

  <div class="modal-header">
    <h4 class="modal-title" id="myModalLabel">Excluir Serviço</h4>

    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <p>Deseja realmente excluir o Serviço?</p>
  </div>
  <div class="modal-footer">
    <form action="/PetShop/app/controllers/servico/excluir.php" method="post">
      <input id="srv_codigo_excluir" type="text" name="srv_codigo_excluir" value="" style="display: none;">
      <button type="reset" class="btn btn-secondary pull-right mx-2" data-dismiss="modal" aria-label="Close" >Cancelar</button>
      <button type="submit" class="btn btn-danger pull-right">Excluir</button>
    </form>
  </div>

  <?php endif; ?>
<?php endif; ?>
