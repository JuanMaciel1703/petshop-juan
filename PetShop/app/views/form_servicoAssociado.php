<div class="row">
  <div class="col-md-6 div-associacao">
    <?php
      require_once(BASE_PATH.'/app/db/db_pet.php');
      $dbPet = new db_pet();

      $pets = $dbPet->getPets();
     ?>
    <fieldset class="form-group">
      <label for="pets">Pet <i class="text-danger">*</i> </label>
      <select id="pets" class="form-control" name="pet" required>
        <option value="">Selecione um Pet</option>
        <?php foreach ($pets as $key => $pet): ?>
          <option value=<?php echo $pet['pet_codigo'] ?>><?php echo $pet['pet_nome'] ?></option>
        <?php endforeach; ?>
      </select>
      <small class="text-muted">Selecione um Pet.</small>
    </fieldset>
  </div>

  <div class="col-md-6 div-associacao">
    <fieldset class="form-group">
      <?php
        require_once(BASE_PATH.'/app/db/db_servico.php');
        $dbServico = new db_servico();

        $servicos = $dbServico->getServicos();
       ?>

      <label for="pets">Serviço <i class="text-danger">*</i></label>
      <select id="servicos" class="form-control" name="servico" required>
        <option value="">Selecione um Serviço</option>
        <?php foreach ($servicos as $key => $servico): ?>
          <option value="<?php echo $servico['srv_codigo'] ?>"><?php echo $servico['srv_descricao'] ?></option>
        <?php endforeach; ?>
      </select>
      <small class="text-muted">Selecione um Serviço.</small>

    </fieldset>

  </div>

  <div class="col-md-6 div-novo" style="display:none">
    <?php
      require_once(BASE_PATH.'/app/db/db_pet.php');
      $dbPet = new db_pet();

      $pets = $dbPet->getPets();
     ?>
    <fieldset class="form-group">
      <label for="pets">Pet <i class="text-danger">*</i> </label>
      <select id="novo_pet" class="form-control" name="novo_pet">
        <option value="">Selecione um Pet</option>
        <?php foreach ($pets as $key => $pet): ?>
          <option value=<?php echo $pet['pet_codigo'] ?>><?php echo $pet['pet_nome'] ?></option>
        <?php endforeach; ?>
      </select>
      <small class="text-muted">Selecione um Pet.</small>
    </fieldset>
  </div>

  <div class="col-md-6 div-novo" style="display:none">
    <fieldset class="form-group">
      <?php
        require_once(BASE_PATH.'/app/db/db_servico.php');
        $dbServico = new db_servico();

        $servicos = $dbServico->getServicos();
       ?>

      <label for="pets">Serviço <i class="text-danger">*</i></label>
      <select id="novo_servico" class="form-control" name="novo_servico">
        <option value="">Selecione um Serviço</option>
        <?php foreach ($servicos as $key => $servico): ?>
          <option value="<?php echo $servico['srv_codigo'] ?>"><?php echo $servico['srv_descricao'] ?></option>
        <?php endforeach; ?>
      </select>
      <small class="text-muted">Selecione um Serviço.</small>

    </fieldset>

  </div>
</div>
