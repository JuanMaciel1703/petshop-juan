<?php include '../../init.php'; ?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Cadastro de Cliente</title>
    <?php require_once(BASE_PATH.'/base/base_header.php'); ?>
    <script src="/PetShop/assets/vendor/JqueryMask/jquery.mask.min.js" charset="utf-8"></script>
  </head>
  <body>
    <?php require_once(BASE_PATH.'/base/header.php'); ?>


    <div class="container">
      <div class="page-header">
        <h1>Cadastro de Cliente</h1>
      </div>
      <hr>
      <form action="/PetShop/app/controllers/cliente/cadastro.php" method="post">

        <?php require_once(BASE_PATH.'/app/views/form_cliente.php') ?>
        <hr>

        <button type="submit" class="btn btn-success pull-right">Cadastrar</button>
        <button type="reset" class="btn btn-danger pull-right mx-2">Limpar</button>

      </form>
    </div>


    <?php require_once(BASE_PATH.'/base/footer_scripts.php'); ?>
  </body>
</html>
