<div class="row">
  <div class="col-md-7">

    <fieldset class="form-group" style="display: none;">
      <input id="srv_codigo_editar" type="text" class="form-control" name="srv_codigo">
    </fieldset>

    <fieldset class="form-group">
      <label for="descricao">Descrição <i class="text-danger">*</i></label>
      <input id="descricao" type="text" class="form-control" placeholder="Descrição" name="descricao" required>
      <small class="text-muted">Descreva o Serviço.</small>
    </fieldset>
  </div>

  <div class="col-md-5">
    <fieldset class="form-group">
      <label for="preco">Preço <i class="text-danger">*</i></label>
      <div class="row">
        <div class="col-lg-6">
          <div class="input-group">
            <span class="input-group-btn">
              <button class="btn btn-secondary" type="button">R$</button>
            </span>
            <input id="preco" type="text" class="form-control" placeholder="0000,00" name="preco" required>
          </div>
        </div>
      </div>
      <small class="text-muted">Digite o preço do serviço.</small>
      <script type="text/javascript">
        $(document).ready(function() {
          $('#preco').mask('0000.00', {reverse:'true'});
        });
      </script>
    </div>
  </fieldset>
</div>
