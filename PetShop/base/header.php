<nav class="navbar navbar-toggleable-sm navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand mx-3" href="/PetShop/index.php"><img src="/PetShop/assets/img/logo.png" class="img-fluid pull-xs-left" alt="logo" width="50px"> <span class="mr-5">PetShop</span> </a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    <ul class="navbar-nav mr-auto mt-2 mt-md-0">
      <li class="nav-item active">
        <a class="nav-link" href="/PetShop/index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/PetShop/app/views/clientes.php">Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/PetShop/app/views/pets.php">Pets</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/PetShop/app/views/servicos.php">Serviços</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/PetShop/app/views/servicos_associados.php">Serviços Associados</a>
      </li>
    </ul>
  </div>
</nav>
<br>
