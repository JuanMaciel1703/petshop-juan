<?php require_once('init.php'); ?>

<!DOCTYPE html>
<html>
  <head>
    <title>PetShop</title>
    <?php require_once('base/base_header.php'); ?>

  </head>
  <body>
    <?php require_once('base/header.php'); ?>

    <div class="container">
      <div class="row">
        <div class="card-deck-wrapper">
          <div class="card-deck">
            <div class="col-md-3">
              <div class="card card-success text-white mb-2">
                <div class="text-center mt-5 mb-5">
                  <i class="fa fa-user-circle fa-5x" aria-hidden="true"></i>
                </div>
                <div class="card-block">
                  <h5 class="card-title">Cadastro de Cliente</h5>
                  <hr>

                  <p class="card-text">
                    Realize o cadastro de um cliente no sistema.
                  </p>
                </div>
                <div class="card-footer text-center">
                  <a class="btn btn-secondary" href="app/views/cadastro_cliente.php" role="button">Cadastrar Cliente</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card card-warning text-white mb-2">
                <div class="text-center mt-5 mb-5">
                  <i class="fa fa-paw fa-5x" aria-hidden="true"></i>
                </div>
                <div class="card-block">
                  <h5 class="card-title">Cadastro de Pet</h5>
                  <hr>

                  <p class="card-text">
                    Realize o cadastro de um animal no sistema.
                  </p>
                </div>
                <div class="card-footer text-center">
                  <a class="btn btn-secondary" href="app/views/cadastro_pet.php" role="button">Cadastrar Pet</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card card-danger text-white mb-2">
                <div class="text-center mt-5 mb-5">
                  <i class="fa fa-tasks fa-5x" aria-hidden="true"></i>
                </div>
                <div class="card-block">
                  <h5 class="card-title">Cadastro de Serviço</h5>
                  <hr>
                  <p class="card-text">
                    Realize o cadastro de um serviço no sistema.
                  </p>
                </div>
                <div class="card-footer text-center">
                  <a class="btn btn-secondary" href="app/views/cadastro_servico.php" role="button">Cadastrar Serviço</a>
                </div>
              </div>
            </div>

            <div class="col-md-3">
              <div class="card card-primary text-white mb-2">
                <div class="text-center mt-5 mb-5">
                  <i class="fa fa-sitemap fa-5x" aria-hidden="true"></i>
                </div>
                <div class="card-block">
                  <h5 class="card-title">Associação de Serviço</h5>
                  <hr>
                  <p class="card-text">
                    Associe um serviço à um Pet cadastrado no sistema.
                  </p>
                </div>
                <div class="card-footer text-center">
                  <a class="btn btn-secondary" href="app/views/associar_servico.php" role="button">Associar Serviço</a>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>

    <?php include('base/footer_scripts.php'); ?>
  </body>
</html>
